package org.example;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/cat")
public class CatServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        int weight = Integer.parseInt(request.getParameter("weight"));
        String isAngry;
        if (Boolean.parseBoolean(request.getParameter("isAngry")))
            isAngry = "Злой";
        else isAngry = "Дружелюбный";

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter writer = response.getWriter();
        writer.write(String.format("<h1>%s кот %s весом %d кг.</h1>", isAngry, name, weight));

    }
}
