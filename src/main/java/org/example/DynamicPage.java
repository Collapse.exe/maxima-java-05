package org.example;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DynamicPage {
    Cat cat;

    public DynamicPage(Cat cat) {
        this.cat = cat;
    }

    public void createPage(String fileName) throws IOException, TemplateException {
        String isAngry = cat.isAngry() ? "злой" : "добрый";
        FileWriter writer = new FileWriter(fileName, false);
        String resourcesPath = "templates";

        Configuration config = new Configuration(Configuration.VERSION_2_3_31);
        config.setDirectoryForTemplateLoading(new File(resourcesPath));
        config.setDefaultEncoding("UTF-8");

        Map<String, String> root = new HashMap<>();
        root.put("title", String.format("Кот %s", cat.getName()));
        root.put("name", cat.getName());
        root.put("weight", String.valueOf(cat.getWeight()));
        root.put("isAngry", isAngry);

        Template template = config.getTemplate("index.html");
        template.process(root, writer);

        writer.flush();
        writer.close();
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }
}
